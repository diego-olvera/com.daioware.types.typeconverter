package typeConverter;

import org.junit.Test;

import com.daioware.commons.wrapper.WrapperInt;
import com.daioware.types.typeConverter.TypeConverter;

import junit.framework.TestCase;

public class FullInstanceConverterTest extends TestCase{

	@Test
	public void test() {
		String string1=new String("hello");
		byte bytes[]=string1.getBytes();
		int begginingPos=0;
		WrapperInt pointer=new WrapperInt();
		TypeConverter converter=new TypeConverter();
		bytes=converter.toBytes(string1);
		String string2=converter.toString(bytes, begginingPos, pointer);
		assertEquals(string1, string2);
		assertEquals(string2.getBytes().length+Integer.BYTES+begginingPos,pointer.value.intValue());
	}
	
}
