package typeConverter;

import org.junit.Test;

import com.daioware.types.typeConverter.ByteToTypeConverter;
import com.daioware.types.typeConverter.BytesList;
import com.daioware.types.typeConverter.TypeConverter;
import com.daioware.types.typeConverter.TypeToByteConverter;

import junit.framework.TestCase;

public class TypeToByteConverterTest extends TestCase{

	@Test
	public void test() {
		int valueIntegerWritten=10, valueIntegerRead;
		String string1="Diego",string2="Diego 2";
		TypeToByteConverter encoder=new TypeToByteConverter(new BytesList(0),new TypeConverter());
		ByteToTypeConverter decoder=new ByteToTypeConverter(encoder);
		encoder.writeInteger(valueIntegerWritten);
		assertEquals(Integer.BYTES*2, encoder.getBytesList().getCurrentSize());
		valueIntegerRead=decoder.readInteger();
		assertEquals(valueIntegerWritten,valueIntegerRead);
		int currentIndex;
		byte bytes[]=encoder.getBytesList().getTrimBytes();
		assertEquals(4, bytes.length);
		currentIndex=encoder.getCurrentIndex();
		encoder.writeString(string1);
		assertEquals(string1,decoder.readString());
		assertEquals(currentIndex+=Integer.BYTES+string1.length(),encoder.getCurrentIndex());
		encoder.writeString(string2);
		assertEquals(currentIndex+=Integer.BYTES+string2.length(),encoder.getCurrentIndex());
		assertEquals(string2,decoder.readString());
	}
}
