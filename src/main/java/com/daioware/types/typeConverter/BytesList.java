package com.daioware.types.typeConverter;

public class BytesList {

	private byte bytes[];
	private int currentIndex;
	
	public BytesList(int initialSize) {
		bytes=new byte[initialSize];
		currentIndex=0;
	}
	
	public BytesList() {
		this(100);
	}
	
	protected void increaseSize(int newSize) {
		byte bytesAux[]=new byte[newSize];
		int i=0;
		for(byte byteAux:bytes) {
			bytesAux[i++]=byteAux;
		}
		bytes=null;
		bytes=bytesAux;
	}
	
	public boolean add(byte byteToAdd) {
		if(currentIndex>=bytes.length) {
			increaseSize(bytes.length*2);
		}
		bytes[currentIndex++]=byteToAdd;
		return true;
	}
	
	public byte[] getBytes() {
		return bytes;
	}

	public byte[] getTrimBytes() {
		int currentIndex=getCurrentIndex();
		byte bytes[]=getBytes();
		byte bytesToReturn[]=new byte[currentIndex];
		for(int i=0;i<currentIndex;i++) {
			bytesToReturn[i]=bytes[i];
		}
		return bytesToReturn;
	}
	
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public int getCurrentSize() {
		return bytes.length;
	}
	
	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	
	public int append(byte bytesToAppend[]) {
		return append(bytesToAppend,0,bytesToAppend.length);
	}
	
	public int append(byte bytesToAppend[],int startIndex) {
		return append(bytesToAppend,startIndex,bytesToAppend.length);
	}
	
	public int append(byte bytesToAppend[],int startIndex, int offset) {
		int bytesAppended=0;
		for(int i=startIndex,j=bytesToAppend.length;bytesAppended<offset && i<j;bytesAppended++,i++) {
			if(!add(bytesToAppend[i])) {
				break;
			}
		}
		return bytesAppended;
	}

	public void increaseSizeAppropiately(int bytesToAppend) {
		int currentIndex=getCurrentIndex();
		int currentSize=getCurrentSize();
		int newSizeRequired=currentIndex+bytesToAppend;
		if(newSizeRequired>=currentSize) {
			increaseSize(newSizeRequired*2);
		}
	}

}
