package com.daioware.types.typeConverter;

import java.nio.ByteOrder;
import java.nio.charset.Charset;

import com.daioware.commons.wrapper.WrapperInt;

public class TypeConverter {

	private Charset charset;
	public ByteOrder order;
	
	public TypeConverter() {
		this(FullConverter.DEFAULT_CHARSET,FullConverter.DEFAULT_ORDER);
	}

	public TypeConverter(Charset charset) {
		this(charset,FullConverter.DEFAULT_ORDER);
	}

	public TypeConverter(ByteOrder order) {
		this(FullConverter.DEFAULT_CHARSET,order);
	}

	public TypeConverter(Charset charset, ByteOrder order) {
		setCharset(charset);
		setOrder(order);
	}

	public Charset getCharset() {
		return charset;
	}

	public ByteOrder getOrder() {
		return order;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	public void setOrder(ByteOrder order) {
		this.order = order;
	}
	
	public  String toBinary(int n){
		return Integer.toString(n, 2);
	}

	public  byte[] toBytes(String s){
		return FullConverter.toBytes(s,getCharset(),getOrder());
	}

	public int toBytes(byte[] bytes,int beginPos,String s){
		return FullConverter.toBytes(bytes,beginPos,s,getCharset(),getOrder());
	}
	
	public  String toString(byte[] bytes,int beginPos){
		return FullConverter.toString(bytes,beginPos,getCharset(),getOrder());
	}
	
	public String toString(byte[] bytes,int beginPos, WrapperInt pointer){
		return FullConverter.toString(bytes,beginPos,getCharset(),getOrder(), pointer);
	}
	
	public  String toString(byte[] bytes){
		return FullConverter.toString(bytes,getCharset(),getOrder());
	}
		
	public  void toBytes(byte buffer[],int beginningPos,int x){
		FullConverter.toBytes(buffer,beginningPos,x, getOrder());
	}
	
	public  void toBytes(byte buffer[],int beginningPos,short x){
		FullConverter.toBytes(buffer,beginningPos,x,getOrder());
	}

	public  void toBytes(byte buffer[],int beginningPos,long x){
		FullConverter.toBytes(buffer,beginningPos,x,getOrder());
	}
	
	public  void toBytes(byte buffer[],int beginningPos,float x){
		FullConverter.toBytes(buffer,beginningPos,x,getOrder());
	}
	
	public  void toBytes(byte buffer[],int beginningPos,double x){
		FullConverter.toBytes(buffer,beginningPos,x,getOrder());
	}
	
	public byte[] toBytes(int myInteger){
		return FullConverter.toBytes(myInteger,getOrder());
	}
	
	public   byte[] toBytes(short s){
		return FullConverter.toBytes(s,getOrder());
	}

	public  int toInt(byte [] bytes){
		return FullConverter.toInt(bytes,getOrder());
	}
	
	public  short toShort(byte [] bytes){
		return FullConverter.toShort(bytes,getOrder());
	}

	public  byte[] toBytes(float f){
		return FullConverter.toBytes(f,getOrder());
	}
	
	public  float toFloat(byte[] bytes){
		return FullConverter.toFloat(bytes,getOrder());
	}

	public  byte[] toBytes(long l) {
		return FullConverter.toBytes(l,getOrder());
	}

	public  long toLong(byte[] bytes) {
		return FullConverter.toLong(bytes,getOrder());
	}

	public  byte[] toBytes(double value) {
		return FullConverter.toBytes(value,getOrder());
	}

	public  long toLong(byte[] bytes,int beginPos){	
		return FullConverter.toLong(bytes,beginPos,getOrder());
	}
	
	public  short toShort(byte[] bytes,int beginPos){
		return FullConverter.toShort(bytes,beginPos,getOrder());
	}
	
	public  int toInt(byte[] bytes,int beginPos){
		return FullConverter.toInt(bytes,beginPos,getOrder());
	}

	public  Double toDouble(byte[] bytes,int beginPos){
		return FullConverter.toDouble(bytes,beginPos,getOrder());
	}
	
	public  Float toFloat(byte[] bytes,int beginPos){
		return FullConverter.toFloat(bytes,beginPos,getOrder());
	}
	
	public  double toDouble(byte[] bytes) {
		return FullConverter.toDouble(bytes,getOrder());
	}
	
}