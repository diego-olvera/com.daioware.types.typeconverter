package com.daioware.types.typeConverter;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

import com.daioware.commons.wrapper.WrapperInt;

public class FullConverter {
	
	public static final Charset DEFAULT_CHARSET=Charset.forName("utf-8");
	public static final ByteOrder DEFAULT_ORDER=ByteOrder.BIG_ENDIAN;
	
	public static ByteOrder order=DEFAULT_ORDER;
	public static Charset charset=DEFAULT_CHARSET;

	public static String toBinary(int n){
		return Integer.toString(n, 2);
	}
	
	public static byte[] toBytes(String s){
		return toBytes(s,order);
	}
	
	public static byte[] toBytes(String s,ByteOrder order){
		return toBytes(s,DEFAULT_CHARSET,order);
	}
	
	public static byte[] toBytes(String s,Charset charset){
		return toBytes(s,charset,order);
	}
	
	public static byte[] toBytes(String s,Charset charset,ByteOrder order){
		byte[] bytesString=s.getBytes(charset);
		byte[] size=toBytes(bytesString.length,order);
		byte[] bytes=new byte[size.length+bytesString.length];
		int p=0;
		for(byte b:size){
			bytes[p++]=b;
		}
		for(byte b:bytesString){
			bytes[p++]=b;
		}
		return bytes;
	}

	public static void toBytes(byte[] bytes,int beginPos,String s){
		toBytes(bytes,beginPos,s,order);
	}
	
	public static int toBytes(byte[] bytes,int beginPos,String s,ByteOrder order){
		return toBytes(bytes,beginPos,s,DEFAULT_CHARSET, order);
	}

	public static int toBytes(byte[] bytes,int beginPos,String s,Charset charset){
		return toBytes(bytes,beginPos,s,charset,order);
	}
	
	public static int toBytes(byte[] bytes,int beginPos,String s,Charset charset,ByteOrder order){
		int p=beginPos;
		byte bytesRead[]=toBytes(s,charset,order);
		int bytesWritten=bytesRead.length;
		for(byte b:bytesRead){
			bytes[p++]=b;
		}
		return bytesWritten;
	}
	
	public static String toString(byte[] bytes,int beginPos){
		return toString(bytes,beginPos);
	}
	
	public static String toString(byte[] bytes,int beginPos,ByteOrder order){
		return toString(bytes,beginPos,DEFAULT_CHARSET,order);
	}
	
	public static String toString(byte[] bytes,int beginPos,Charset charset,ByteOrder order){
		return toString(bytes,beginPos,charset,order,new WrapperInt());
	}
	
	public static String toString(byte[] bytes,int beginPos,Charset charset,ByteOrder order, WrapperInt pointer){
		int size=FullConverter.toInt(bytes,beginPos,order);
		int p=beginPos;
		byte bytesNew[]=new byte[size+Integer.BYTES];
		for(int i=0,j=bytesNew.length;i<j;i++){
			bytesNew[i]=bytes[p++];
		}
		pointer.value=beginPos+bytesNew.length;
		return FullConverter.toString(bytesNew,charset,order);
	}

	public static String toString(byte[] bytes){
		return toString(bytes,order);
	}
	
	public static String toString(byte[] bytes,ByteOrder order){
		return toString(bytes,DEFAULT_CHARSET,order);
	}
	
	public static String toString(byte[] bytes, Charset charset){
		return toString(bytes,charset);
	}
	
	public static String toString(byte[] bytes, Charset charset,ByteOrder order){
		byte[] sizeBytes=new byte[Integer.BYTES];
		int currentBytesPos,i;
		for(i=currentBytesPos=0;i<Integer.BYTES;i++){
			sizeBytes[i]=bytes[currentBytesPos++];
		}
		int size=toInt(sizeBytes,order);
		byte[] stringBytes=new byte[size];
		for(i=0;i<size;i++){
			stringBytes[i]=bytes[currentBytesPos++];
		}
		return new String(stringBytes,charset);	
	}
	
	public static void toBytes(byte buffer[],int beginningPos,int x){
		toBytes(buffer,beginningPos,x, order);
	}
	
	public static void toBytes(byte buffer[],int beginningPos,int x,ByteOrder order){
		int i=beginningPos;
		for(byte b: toBytes(x,order)){
			buffer[i++]=b;
		}
	}
	
	public static void toBytes(byte buffer[],int beginningPos,short x){
		toBytes(buffer,beginningPos,x,order);
	}
	
	public static void toBytes(byte buffer[],int beginningPos,short x,ByteOrder order){
		int i=beginningPos;
		for(byte b: toBytes(x,order)){
			buffer[i++]=b;
		}
	}

	public static void toBytes(byte buffer[],int beginningPos,long x){
		toBytes(buffer,beginningPos,x,order);
	}
	
	public static void toBytes(byte buffer[],int beginningPos,long x,ByteOrder order){
		int i=beginningPos;
		for(byte b: toBytes(x,order)){
			buffer[i++]=b;
		}
	}

	public static void toBytes(byte buffer[],int beginningPos,float x){
		toBytes(buffer,beginningPos,x,order);
	}
	
	public static void toBytes(byte buffer[],int beginningPos,float x,ByteOrder order){
		int i=beginningPos;
		for(byte b: toBytes(x,order)){
			buffer[i++]=b;
		}
	}
	
	public static void toBytes(byte buffer[],int beginningPos,double x){
		toBytes(buffer,beginningPos,x,order);
	}
	
	public static void toBytes(byte buffer[],int beginningPos,double x,ByteOrder order){
		int i=beginningPos;
		for(byte b: toBytes(x,order)){
			buffer[i++]=b;
		}
	}

	public static byte[] toBytes(int myInteger){
		return toBytes(myInteger,order);
	}
	
	public static byte[] toBytes(int myInteger,ByteOrder order){
	    return ByteBuffer.allocate(4).order(order).putInt(myInteger).array();
	}

	public static  byte[] toBytes(short s){
		return toBytes(s,order);
	}
	
	public static  byte[] toBytes(short s,ByteOrder order){
	    return ByteBuffer.allocate(2).order(order).putShort(s).array();
	}

	public static int toInt(byte [] bytes){
		return toInt(bytes,order);
	}
	
	public static int toInt(byte [] bytes,ByteOrder order){
	    return ByteBuffer.wrap(bytes).order(order).getInt();
	}

	public static short toShort(byte [] bytes){
		return toShort(bytes,order);
	}
	
	public static short toShort(byte [] bytes,ByteOrder order){
	    return ByteBuffer.wrap(bytes).order(order).getShort();
	}
	
	public static byte[] toBytes(float f){
		return toBytes(f,order);
	}
	
	public static byte[] toBytes(float f,ByteOrder order){
		return ByteBuffer.allocate(4).order(order).putFloat(f).array();
	}

	public static float toFloat(byte[] bytes){
		return toFloat(bytes,order);
	}
	
	public static float toFloat(byte[] bytes, ByteOrder order){
		return ByteBuffer.wrap(bytes).order(order).getFloat();
	}

	public static byte[] toBytes(long l) {
		return toBytes(l,order);
	}
	
	public static byte[] toBytes(long l,ByteOrder order) {
		return ByteBuffer.allocate(8).order(order).putLong(l).array();
	}

	public static long toLong(byte[] bytes) {
		return toLong(bytes,order);
	}
	
	public static long toLong(byte[] bytes,ByteOrder order) {
		return ByteBuffer.wrap(bytes).order(order).getLong();
	}

	public static byte[] toBytes(double value) {
		return toBytes(value,order);
	}
	
	public static byte[] toBytes(double value,ByteOrder order) {
		return ByteBuffer.allocate(8).order(order).putDouble(value).array();
	}
	
	public static long toLong(byte[] bytes,int beginPos){	
		return toLong(bytes,beginPos,order);
	}
	
	public static long toLong(byte[] bytes,int beginPos,ByteOrder order){
	    return ByteBuffer.wrap(bytes).order(order).getLong(beginPos);
	}

	public static short toShort(byte[] bytes,int beginPos){
		return toShort(bytes,beginPos);
	}
	
	public static short toShort(byte[] bytes,int beginPos,ByteOrder order){
	    return ByteBuffer.wrap(bytes).order(order).getShort(beginPos);
	}

	public static int toInt(byte[] bytes,int beginPos){
		return toInt(bytes,beginPos,order);
	}

	public static int toInt(byte[] bytes,int beginPos,ByteOrder order){
	    return ByteBuffer.wrap(bytes).order(order).getInt(beginPos);
	}

	public static Double toDouble(byte[] bytes,int beginPos){
		return toDouble(bytes,beginPos,order);
	}
	
	public static Double toDouble(byte[] bytes,int beginPos,ByteOrder order){
	    return ByteBuffer.wrap(bytes).order(order).getDouble(beginPos);
	}

	public static Float toFloat(byte[] bytes,int beginPos){
		return toFloat(bytes,beginPos,order);
	}
	
	public static Float toFloat(byte[] bytes,int beginPos,ByteOrder order){
	    return ByteBuffer.wrap(bytes).order(order).getFloat(beginPos);
	}
	
	public static double toDouble(byte[] bytes) {
		return toDouble(bytes,order);
	}
	
	public static double toDouble(byte[] bytes,ByteOrder order) {
	    return ByteBuffer.wrap(bytes).order(order).getDouble();
	}
}