package com.daioware.types.typeConverter;

public class TypeToByteConverter {

	private BytesList bytesList;
	private TypeConverter typeConverter;
	
	public TypeToByteConverter(BytesList bytesList, TypeConverter typeConverter) {
		setBytesList(bytesList);
		setTypeConverter(typeConverter);
	}
	
	public BytesList getBytesList() {
		return bytesList;
	}

	public TypeConverter getTypeConverter() {
		return typeConverter;
	}

	public void setBytesList(BytesList bytesList) {
		this.bytesList = bytesList;
	}

	public void setTypeConverter(TypeConverter typeConverter) {
		this.typeConverter = typeConverter;
	}

	public void setCurrentIndex(int currentIndex) {
		bytesList.setCurrentIndex(currentIndex);
	}
	
	public int getCurrentIndex() {
		return bytesList.getCurrentIndex();
	}
	
	public void writeInteger(int value) {
		bytesList.increaseSizeAppropiately(Integer.BYTES);
		int currentIndex=getCurrentIndex();
		typeConverter.toBytes(getBytesList().getBytes(),currentIndex,value);
		setCurrentIndex(currentIndex+Integer.BYTES);
	}
	
	public void writeShort(short value) {
		bytesList.increaseSizeAppropiately(Short.BYTES);
		int currentIndex=getCurrentIndex();
		typeConverter.toBytes(getBytesList().getBytes(),currentIndex,value);
		setCurrentIndex(currentIndex+Short.BYTES);
	}
	
	public void writeLong(long value) {
		bytesList.increaseSizeAppropiately(Long.BYTES);
		int currentIndex=getCurrentIndex();
		typeConverter.toBytes(getBytesList().getBytes(),currentIndex,value);
		setCurrentIndex(currentIndex+Short.BYTES);
	}
	
	public void writeFloat(float value) {
		bytesList.increaseSizeAppropiately(Long.BYTES);
		int currentIndex=getCurrentIndex();
		typeConverter.toBytes(getBytesList().getBytes(),currentIndex,value);
		setCurrentIndex(currentIndex+Short.BYTES);
	}
	
	public void writeString(String value) {
		bytesList.append(typeConverter.toBytes(value));
	}
	
	
	public void writeByte(byte value) {
		getBytesList().add(value);
	}
	
}
