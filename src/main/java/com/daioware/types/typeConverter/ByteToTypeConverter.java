package com.daioware.types.typeConverter;

import com.daioware.commons.wrapper.WrapperInt;

public class ByteToTypeConverter {

	private TypeToByteConverter typeToByteConverter;
	private int currentIndex;
	
	public ByteToTypeConverter(TypeToByteConverter typeToByteConverter) {
		setTypeToByteConverter(typeToByteConverter);
	}
	
	public int getCurrentIndex() {
		return currentIndex;
	}


	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}


	public TypeToByteConverter getTypeToByteConverter() {
		return typeToByteConverter;
	}


	public void setTypeToByteConverter(TypeToByteConverter typeToByteConverter) {
		this.typeToByteConverter = typeToByteConverter;
	}

	
	public int readInteger() {
		TypeToByteConverter conv=getTypeToByteConverter();
		TypeConverter typeConv=conv.getTypeConverter();
		BytesList bytesList=conv.getBytesList();
		int currentIndex=getCurrentIndex();
		int valueRead;
		valueRead=typeConv.toInt(bytesList.getBytes(),currentIndex);
		setCurrentIndex(currentIndex+Integer.BYTES);
		return valueRead;
	}
	
	public short readShort() {
		TypeToByteConverter conv=getTypeToByteConverter();
		TypeConverter typeConv=conv.getTypeConverter();
		BytesList bytesList=conv.getBytesList();
		int currentIndex=getCurrentIndex();
		short valueRead;
		valueRead=typeConv.toShort(bytesList.getBytes(),currentIndex);
		setCurrentIndex(currentIndex+Short.BYTES);
		return valueRead;
	}
	
	public long readLong() {
		TypeToByteConverter conv=getTypeToByteConverter();
		TypeConverter typeConv=conv.getTypeConverter();
		BytesList bytesList=conv.getBytesList();
		int currentIndex=getCurrentIndex();
		long valueRead;
		valueRead=typeConv.toLong(bytesList.getBytes(),currentIndex);
		setCurrentIndex(currentIndex+Long.BYTES);
		return valueRead;
	}
	
	public float readFloat() {
		TypeToByteConverter conv=getTypeToByteConverter();
		TypeConverter typeConv=conv.getTypeConverter();
		BytesList bytesList=conv.getBytesList();
		int currentIndex=getCurrentIndex();
		float valueRead;
		valueRead=typeConv.toFloat(bytesList.getBytes(),currentIndex);
		setCurrentIndex(currentIndex+Float.BYTES);
		return valueRead;
	}
	
	public byte readByte() {
		int currentIndex=getCurrentIndex();
		byte valueRead=typeToByteConverter.getBytesList().getBytes()[getCurrentIndex()];
		setCurrentIndex(currentIndex+1);
		return valueRead;
	}
		
	public String readString() {
		TypeToByteConverter conv=getTypeToByteConverter();
		TypeConverter typeConv=conv.getTypeConverter();
		BytesList bytesList=conv.getBytesList();
		int currentIndex=getCurrentIndex();
		WrapperInt pointer=new WrapperInt();
		String valueRead=typeConv.toString(bytesList.getBytes(),currentIndex, pointer);
		setCurrentIndex(pointer.value);
		return valueRead;
	}
}
